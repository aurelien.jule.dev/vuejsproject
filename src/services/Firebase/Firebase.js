import firebase from 'firebase'

var config = {
  apiKey: 'AIzaSyBqhO6SjOyOv6m7C3AsoPeggSKVhMRBTfA',
  authDomain: 'vuejsproject-9e6f3.firebaseapp.com',
  projectId: 'vuejsproject-9e6f3'
}

firebase.initializeApp(config)
var db = firebase.firestore()
db.settings({
  timestampsInSnapshots: true
})

export default {
  getMessages () {
    return new Promise((resolve, reject) => {
      db.collection('/channels/rvBhAEseoaZK8P8trwx2/openChannels/rIWifIa0NAEwjUTs4ICD/channel1').orderBy('date', 'asc').get().then((querySnapshot) => {
        resolve(querySnapshot.docs.map(function (documentSnapshot) { return documentSnapshot.data() }))
      }).catch((error) => {
        console.log('Error getting documents: ', error)
      })
    }, error => {
      Promise.reject(error)
    })
  },

  sendMessage (login, message) {
    return new Promise((resolve, reject) => {
      db.collection('/channels/rvBhAEseoaZK8P8trwx2/openChannels/rIWifIa0NAEwjUTs4ICD/channel1').add({
        userID: 'userID',
        userName: login,
        message: message,
        date: Date.now()
      }).then(() => {
        console.log('Document successfully written!')
      }).catch((error) => {
        console.log('Error writing document: ', error)
      })
    }, error => {
      Promise.reject(error)
    })
  }
}
